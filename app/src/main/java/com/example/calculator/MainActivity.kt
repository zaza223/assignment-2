package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private  var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)
    }
    fun numberClick(clickedView: View){
        if(clickedView is TextView){
            val number = clickedView.text.toString()
            var result = resultTextView.text.toString()

            if(result == "0")
                result = ""

            resultTextView.text = result+number
        }
    }
    fun operationClick(clickedView: View){
        if(clickedView is TextView){
            this.operand = resultTextView.text.toString().toDouble()
            this.operation = clickedView.text.toString()
            resultTextView.text = ""
        }
    }
    fun  equalsClick(clickedView: View){
        val secOperand = resultTextView.text.toString().toDouble()

        when(operation){
            "/" -> resultTextView.text = (operand/secOperand).toString()
            "*" -> resultTextView.text = (operand*secOperand).toString()
            "-" -> resultTextView.text = (operand-secOperand).toString()
            "+" -> resultTextView.text = (operand+secOperand).toString()
        }
    }
    fun delClicked(clickedView: View){
        if(clickedView is TextView){
            var a:String = ""
            if(resultTextView.text.toString().equals("")){
                a="";
            }else
            if(resultTextView.text.toString().length==1){
                a = "0"
            }else{
                a = resultTextView.text.toString().substring(0,resultTextView.text.toString().length-1)
            }

            if(a.get(a.length-1)=='.'){
                a = a.substring(0,a.length-1)
            }
            if(resultTextView.text.toString().length==1){
                a = "0"
            }
            resultTextView.text = a
        }

    }
    fun clearClick(clickedView: View){
        resultTextView.text = "0"
    }
    fun dotClicked(clickedView: View){
        var res = resultTextView.text.toString()
        if(!res.contains(".")){
            resultTextView.text = res + "."
        }

    }
}